package stream;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Runner {

    private static List<User> allUsers;
    static {
        allUsers = new ArrayList<User>();
        User parent;
        for (int i = 1; i < 11; i++) {
            List<User> children = new ArrayList<>();
            parent = new User(i, ThreadLocalRandom.current().nextInt(18, 50), "parent" + i);
            long childrenCount = ThreadLocalRandom.current().nextInt(0, 3);
            if (childrenCount != 0) {
                children = new ArrayList<>();
                for (long count = childrenCount; count > 0; count--) {
                    children.add(new User(100 * parent.getId() + count, ThreadLocalRandom.current().nextInt(0, parent.getAge() - 16), parent.getName() + "child"));
                }
            }
            parent.setChildren(children);
            allUsers.add(parent);
        }
    }

    public static void main(String[] args) {

        System.out.println(childrenCount((ArrayList<User>) allUsers));
        System.out.println(summParentsAge((ArrayList<User>) allUsers));
        System.out.println(parent27Count((ArrayList<User>) allUsers));
        System.out.println(childrenAgeSumm((ArrayList<User>) allUsers));
        sortedChildren((ArrayList<User>) allUsers).forEach(System.out::println);  System.out.println();
        sorted10yearChildren((ArrayList<User>) allUsers).forEach(System.out::println);  System.out.println();
        unicChildrenNames((ArrayList<User>) allUsers).forEach(System.out::println);  System.out.println();
        System.out.println(ifAgeMore17((ArrayList<User>) allUsers));
        System.out.println(ifAllAgeLess30((ArrayList<User>) allUsers));
        System.out.println(maxParentAge((ArrayList<User>) allUsers));
        System.out.println(minChildAge((ArrayList<User>) allUsers));
        System.out.println(averageChildrenAge((ArrayList<User>) allUsers));
        System.out.println(multiParentsAge((ArrayList<User>) allUsers));
        System.out.println(currentParentNameifAge((ArrayList<User>) allUsers,30));

        try{
            System.out.println(firstChildIdifAge((ArrayList<User>) allUsers,1));
        }catch (NoSuchElementException ex){}

        System.out.println(parentsCount18_27((ArrayList<User>) allUsers));

        sortChildrenNameAge((ArrayList<User>) allUsers).forEach(System.out::println);
        sortChildren3_6Age((ArrayList<User>) allUsers).forEach(System.out::println);
        System.out.println(parentsNames((ArrayList<User>) allUsers));

        returnMapIdList((ArrayList<User>) allUsers).entrySet().forEach(System.out::println);
        returnMapUserChildName((ArrayList<User>) allUsers).entrySet().forEach(System.out::println);
        returnMapUserChildMaxAge((ArrayList<User>) allUsers).entrySet().forEach(System.out::println);
    }


    public static int childrenCount(ArrayList<User> allUsers){
        int count = 0;
        count = allUsers.stream().mapToInt(user->user.getChildren().size()).sum();

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return count;
    }

    public static int summParentsAge(ArrayList<User> allUsers){
        int age = 0;
        age = allUsers.stream().mapToInt(User::getAge).sum();

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return age;
    }

    public static long parent27Count(ArrayList<User> allUsers){
        long count = 0;
        count = allUsers.stream().filter(user -> user.getAge() > 27).count();

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return count;
    }

    public static int childrenAgeSumm(ArrayList<User> allUsers){
        int ageSumm = 0;
        ageSumm = allUsers.stream().flatMap(user->user.getChildren().stream())
                .mapToInt(User::getAge).sum();

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return ageSumm;
    }

    public static ArrayList<User> sortedChildren(ArrayList<User> allUsers){
        ArrayList<User> children = new ArrayList<>();
        children = (ArrayList<User>) allUsers.stream().flatMap(user -> user.getChildren().stream())
                .sorted(Comparator.comparingInt(User::getAge))
                .collect(Collectors.toList());

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return children;
    }

    public static ArrayList<User> sorted10yearChildren(ArrayList<User> allUsers){
        ArrayList<User> children = new ArrayList<>();
        children = (ArrayList<User>) allUsers.stream().flatMap(user -> user.getChildren().stream())
                .filter(o -> o.getAge() < 10).collect(Collectors.toList());

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return children;
    }

    public static ArrayList<String> unicChildrenNames(ArrayList<User> allUsers){
        List<String> childrenNames = new ArrayList<>();
        childrenNames = allUsers.stream().flatMap(user -> user.getChildren().stream())
                .map(User::getName).distinct().collect(Collectors.toList());

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return (ArrayList<String>) childrenNames;
    }

    public static boolean ifAgeMore17(ArrayList<User> allUsers){
        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return allUsers.stream().flatMap(user -> user.getChildren().stream())
                .anyMatch(o -> o.getAge() > 17);
    }

    public static boolean ifAllAgeLess30(ArrayList<User> allUsers){
        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return allUsers.stream().flatMap(user -> user.getChildren().stream())
                .allMatch(o -> o.getAge() < 30);
    }

    public static int maxParentAge(ArrayList<User> allUsers){
        int max;
        max = allUsers.stream().mapToInt(User::getAge).max().getAsInt();

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return max;
    }

    public static int minChildAge(ArrayList<User> allUsers){
        int min;
        min = allUsers.stream().flatMap(user -> user.getChildren().stream()).mapToInt(User::getAge).min().getAsInt();

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return min;
    }

    public static double averageChildrenAge(ArrayList<User> allUsers){
        double average;
        average = allUsers.stream().flatMap(user -> user.getChildren().stream()).mapToInt(User::getAge).average().getAsDouble();

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return average;
    }

    public static long multiParentsAge(ArrayList<User> allUsers){
        long average;
        average = allUsers.stream().mapToLong(User::getAge).reduce((o1,o2)->o1*o2).orElse(0);

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return average;
    }

    public static String currentParentNameifAge(ArrayList<User> allUsers, int age){
        String name;
        name = allUsers.stream().filter(user -> user.getAge() < age).map(user -> user.getName()).findFirst().orElse("None");

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return name;
    }

    public static long firstChildIdifAge(ArrayList<User> allUsers, int age){
        long id;
        id = allUsers.stream().flatMap(user -> user.getChildren().stream())
                .filter(user -> user.getAge() < age)
                .mapToLong(User::getId)
                .findFirst().orElseThrow(NoSuchElementException::new);

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return id;
    }

    public static long parentsCount18_27(ArrayList<User> allUsers){
        long count;
        count = allUsers.stream().filter(user -> user.getAge() > 18 && user.getAge() < 27).count();

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return count;
    }

    public static ArrayList<User> sortChildrenNameAge(ArrayList<User> allUsers){
        List<User> sorted = new ArrayList<>();

        sorted = allUsers.stream().flatMap(user -> user.getChildren().stream())
                .sorted(((o1, o2) -> Integer.compare(o1.getAge(), o2.getAge()) == 0
                        ? o1.getName().compareTo(o2.getName())
                        : Integer.compare(o1.getAge(), o2.getAge())))
                .collect(Collectors.toList());

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return (ArrayList<User>) sorted;
    }


    public static ArrayList<User> sortChildren3_6Age(ArrayList<User> allUsers){
        List<User> sorted = new ArrayList<>();
        sorted = allUsers.stream().flatMap(user -> user.getChildren().stream())
                .sorted((Comparator.comparingInt(User::getAge))).skip(2).limit(3).collect(Collectors.toList());

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return (ArrayList<User>) sorted;
    }

    public static String parentsNames(ArrayList<User> allUsers){
        String names;
        names = allUsers.stream().map(User::getName).reduce((o1, o2) -> o1 + ", " + o2).get();

        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return names;
    }

    public static Map<Long, List<User>> returnMapIdList(ArrayList<User> allUsers) {
        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return allUsers.stream().collect(Collectors.toMap(User::getId, User::getChildren));
    }

    public static Map<User, List<String>> returnMapUserChildName(ArrayList<User> allUsers) {
        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return allUsers.stream().collect(Collectors.toMap(user -> user, user->user.getChildren().stream().map(User::getName).collect(Collectors.toList())));
    }

    public static Map<User, Integer> returnMapUserChildMaxAge(ArrayList<User> allUsers) {
        System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        return allUsers.stream().collect(Collectors.toMap(user -> user, user->user.getChildren().stream().mapToInt(User::getAge).max().orElse(-1)));
    }

    }
